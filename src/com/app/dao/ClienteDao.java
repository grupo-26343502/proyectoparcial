package com.app.dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.app.model.Model_Cliente;

public class ClienteDao {
	
	private String url = "jdbc:postgresql://localhost:5432/ProyectoPAG";
	private String usuario = "postgres";
	private String contraseña = "1234";
	
	public List<Model_Cliente>getCliente(){
		List<Model_Cliente> cliente = new ArrayList<>();
		
	try (Connection connection = DriverManager.getConnection(url, usuario, contraseña)) {
        System.out.println("Conexión exitosa a la base de datos");
        
        // Consulta (select) de datos
        String selectQuery = "select * from public.cliente";
        
        try (PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {
        	
            ResultSet resultSet = selectStatement.executeQuery();
            
            while (resultSet.next()) {
                int IdCliente = resultSet.getInt("idcli");
                String nombreCliente = resultSet.getString("nomcli");
                String estadoCliente = resultSet.getString("estado");
                
                Model_Cliente cli = new Model_Cliente(IdCliente, nombreCliente, estadoCliente);
                cliente.add(cli);
                
//                System.out.println(id + ", " + nombre + ", " + cantidad + ", " + precio);
            }
        } catch (SQLException e) {
            System.out.println("Error al ejecutar la consulta: " + e.getMessage());
        }


    } catch (SQLException e) {
        System.out.println("Error al conectarse a la base de datos: " + e.getMessage());
    }
	
	return cliente;
	}
	public Model_Cliente getClienteById(int idcli){
		Model_Cliente cliente = new Model_Cliente();
		
		try (Connection connection = DriverManager.getConnection(url, usuario, contraseña)) {
            System.out.println("Conexión exitosa a la base de datos");
            
            // Consulta (select) de datos
            String selectQuery = "select * from public.cliente where codigo="+idcli;
            
            try (PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {
            	
                ResultSet resultSet = selectStatement.executeQuery();
                
                if (resultSet.next()) {
                    int IdCliente = resultSet.getInt("idcli");
                    String nombreCliente = resultSet.getString("nomcli");
                    String estadoCliente = resultSet.getString("estado");
                    
                    cliente = new Model_Cliente(IdCliente, nombreCliente, estadoCliente);
                }
            } catch (SQLException e) {
                System.out.println("Error al ejecutar la consulta: " + e.getMessage());
            }


        } catch (SQLException e) {
            System.out.println("Error al conectarse a la base de datos: " + e.getMessage());
        }
		
		return cliente;
	}
}

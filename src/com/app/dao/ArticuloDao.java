package com.app.dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.app.model.Model_Articulo;

public class ArticuloDao {
	private String url = "jdbc:postgresql://localhost:5432/ProyectoPAG";
	private String usuario = "postgres";
	private String contraseña = "1234";
	
	public List<Model_Articulo>getArticulo(){
		List<Model_Articulo> articulo = new ArrayList<>();
		
	try (Connection connection = DriverManager.getConnection(url, usuario, contraseña)) {
        System.out.println("Conexión exitosa a la base de datos");
        
        // Consulta (select) de datos
        String selectQuery = "select * from public.articulo";
        
        try (PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {
        	
            ResultSet resultSet = selectStatement.executeQuery();
            
            while (resultSet.next()) {
                int numArtic = resultSet.getInt("numartic");
                String nombreArticulo = resultSet.getString("nomartic");
                double precioArt = resultSet.getDouble("precio");
                
                Model_Articulo art = new Model_Articulo (numArtic, nombreArticulo, precioArt);
                articulo.add(art);
            }
        } catch (SQLException e) {
            System.out.println("Error al ejecutar la consulta: " + e.getMessage());
        }


    } catch (SQLException e) {
        System.out.println("Error al conectarse a la base de datos: " + e.getMessage());
    }
	
	return articulo;
	}
	public Model_Articulo getClienteById(int numartic){
		Model_Articulo articulo = new Model_Articulo();
		
		try (Connection connection = DriverManager.getConnection(url, usuario, contraseña)) {
            System.out.println("Conexión exitosa a la base de datos");
            
            // Consulta (select) de datos
            String selectQuery = "select * from public.articulo where codigo="+numartic;
            
            try (PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {
            	
                ResultSet resultSet = selectStatement.executeQuery();
                
                if (resultSet.next()) {
                    int numArtic = resultSet.getInt("numartic");
                    String nombreArticulo = resultSet.getString("nomartic");
                    double precioArt = resultSet.getDouble("precio");
                    
                    articulo = new Model_Articulo (numArtic, nombreArticulo, precioArt);
                }
            } catch (SQLException e) {
                System.out.println("Error al ejecutar la consulta: " + e.getMessage());
            }


        } catch (SQLException e) {
            System.out.println("Error al conectarse a la base de datos: " + e.getMessage());
        }
		
		return articulo;
	}

}

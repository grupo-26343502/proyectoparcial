package com.app.dao;
import java.sql.Connection;

import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.app.model.Model_OrdenCompra;

public class OrdenCompraDao {

    private String url = "jdbc:postgresql://localhost:5432/ProyectoPAG";
    private String usuario = "postgres";
    private String contraseña = "1234";

    public List<Model_OrdenCompra> getOrdenCompra() {
        List<Model_OrdenCompra> ordenComp = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(url, usuario, contraseña)) {
            System.out.println("Conexión exitosa a la base de datos");

            // Consulta (select) de datos
            String selectQuery = "SELECT * FROM public.ordencompra";

            try (PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {

                ResultSet resultSet = selectStatement.executeQuery();

                while (resultSet.next()) {
                    int IdOrden = resultSet.getInt("idorden");
                    int IdCliente = resultSet.getInt("idcliente");
                    Date fecha = resultSet.getDate("fecha");
                    int cant = resultSet.getInt("cantidad");

                    // Puedes usar directamente el IdCliente en la creación de la OrdenCompra
                    Model_OrdenCompra ordC = new Model_OrdenCompra(IdOrden, IdCliente, fecha, cant);
                    ordenComp.add(ordC);
                }
            } catch (SQLException e) {
                System.out.println("Error al ejecutar la consulta: " + e.getMessage());
            }

        } catch (SQLException e) {
            System.out.println("Error al conectarse a la base de datos: " + e.getMessage());
        }

        return ordenComp;
    }
    public Model_OrdenCompra getOrdenCompraById(int idorden){
    	Model_OrdenCompra ordenCompra = new Model_OrdenCompra();
		
		try (Connection connection = DriverManager.getConnection(url, usuario, contraseña)) {
            System.out.println("Conexión exitosa a la base de datos");
            
            // Consulta (select) de datos
            String selectQuery = "select * from public.ordencompra where codigo="+idorden;
            
            try (PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {
            	
                ResultSet resultSet = selectStatement.executeQuery();
                
                if (resultSet.next()) {
                    int IdOrden = resultSet.getInt("idorden");
                    int IdCliente = resultSet.getInt("idcliente");
                    Date fecha = resultSet.getDate("fecha");
                    int cant = resultSet.getInt("cantidad");

                    // Puedes usar directamente el IdCliente en la creación de la OrdenCompra
                    ordenCompra  = new Model_OrdenCompra(IdOrden, IdCliente, fecha, cant);
                }
            } catch (SQLException e) {
                System.out.println("Error al ejecutar la consulta: " + e.getMessage());
            }


        } catch (SQLException e) {
            System.out.println("Error al conectarse a la base de datos: " + e.getMessage());
        }
		
		return ordenCompra;
	}
}


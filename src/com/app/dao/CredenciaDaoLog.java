package com.app.dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.app.model.Model_CredencialesLogin;
public class CredenciaDaoLog implements CredDaoImp {
	
	String url = "jdbc:postgresql://localhost:5432/ProyectoPAG";
	String usuario = "postgres";
	String contraseña = "1234";
	
	public Model_CredencialesLogin login(String username, String password) {
	    Model_CredencialesLogin user = null;

	    try (Connection connection = DriverManager.getConnection(url, usuario, contraseña)) {
	        System.out.println("Conexión exitosa a la base de datos");

	        String selectQuery = "select * from public.credenciales where usuario = ? and contrasena = ?";

	        try (PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {
	            selectStatement.setString(1, username);   // Establecer el valor del primer parámetro
	            selectStatement.setString(2, password);   // Establecer el valor del segundo parámetro

	            ResultSet rs = selectStatement.executeQuery();
	            if (rs.next()) {
	                int id = rs.getInt("id");
	                user = new Model_CredencialesLogin(username, password,id);
	            }
	        } catch (SQLException e) {
	            System.out.println("Error al ejecutar en la consola: " + e.getMessage());
	        }

	    } catch (SQLException e) {
	        System.out.println("Error al ejecutar la consulta: " + e.getMessage());
	    }
	    return user;
	}

}

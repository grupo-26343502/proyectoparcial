package com.app.dao;
import com.app.model.Model_CredencialesLogin;

public interface CredDaoImp {
	public Model_CredencialesLogin login(String usuario, String contrasena);
}

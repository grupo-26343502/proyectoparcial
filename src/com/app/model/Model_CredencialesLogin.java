package com.app.model;

public class Model_CredencialesLogin {
	
	private String usuario;
	private String contrasena;
	private int id;
	public Model_CredencialesLogin(String usuario, String contrasena, int id) {
		super();
		this.usuario = usuario;
		this.contrasena = contrasena;
		this.id = id;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Model_CredencialesLogin [usuario=" + usuario + ", contrasena=" + contrasena + ", id=" + id + "]";
	}
}

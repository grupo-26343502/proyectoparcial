package com.app.model;

public class Model_Articulo {
	private int numartic;
	private String nomartic;
	private double precio;
	public Model_Articulo() {
		super();
	}
	public Model_Articulo(int numartic, String nomartic, double precio) {
		super();
		this.numartic = numartic;
		this.nomartic = nomartic;
		this.precio = precio;
	}
	public int getNumartic() {
		return numartic;
	}
	public void setNumartic(int numartic) {
		this.numartic = numartic;
	}
	public String getNomartic() {
		return nomartic;
	}
	public void setNomartic(String nomartic) {
		this.nomartic = nomartic;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	@Override
	public String toString() {
		return "Articulo [numartic=" + numartic + ", nomartic=" + nomartic + ", precio=" + precio + "]";
	}
}
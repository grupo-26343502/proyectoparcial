package com.app.model;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Model_ConsultaIds {
    private SimpleIntegerProperty idCli;
    private SimpleIntegerProperty numeroArtic;
    private SimpleStringProperty nomcli;
    private SimpleStringProperty nomartic;

    public Model_ConsultaIds(int idCli, int numeroArtic, String nomcli, String nomartic) {
        this.idCli = new SimpleIntegerProperty(idCli);
        this.numeroArtic = new SimpleIntegerProperty(numeroArtic);
        this.nomcli = new SimpleStringProperty(nomcli);
        this.nomartic = new SimpleStringProperty(nomartic);
    }

    public SimpleIntegerProperty idCliProperty() {
        return idCli;
    }

    public SimpleIntegerProperty numeroArticProperty() {
        return numeroArtic;
    }

    public SimpleStringProperty nomcliProperty() {
        return nomcli;
    }

    public SimpleStringProperty nomarticProperty() {
        return nomartic;
    }

	public SimpleIntegerProperty getIdCli() {
		return idCli;
	}

	public void setIdCli(SimpleIntegerProperty idCli) {
		this.idCli = idCli;
	}

	public SimpleIntegerProperty getNumeroArtic() {
		return numeroArtic;
	}

	public void setNumeroArtic(SimpleIntegerProperty numeroArtic) {
		this.numeroArtic = numeroArtic;
	}

	public SimpleStringProperty getNomcli() {
		return nomcli;
	}

	public void setNomcli(SimpleStringProperty nomcli) {
		this.nomcli = nomcli;
	}

	public SimpleStringProperty getNomartic() {
		return nomartic;
	}

	public void setNomartic(SimpleStringProperty nomartic) {
		this.nomartic = nomartic;
	}
}
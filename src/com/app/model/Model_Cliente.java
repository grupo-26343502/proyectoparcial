package com.app.model;

public class Model_Cliente {
	private int idcli;
	private String nomcli;
	private String estado;
	public Model_Cliente() {
		super();
	}
	public Model_Cliente(int idcli, String nomcli, String estado) {
		super();
		this.idcli = idcli;
		this.nomcli = nomcli;
		this.estado = estado;
	}
	public int getIdcli() {
		return idcli;
	}
	public void setIdcli(int idcli) {
		this.idcli = idcli;
	}
	public String getNomcli() {
		return nomcli;
	}
	public void setNomcli(String nomcli) {
		this.nomcli = nomcli;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	@Override
	public String toString() {
		return "Cliente [idcli=" + idcli + ", nomcli=" + nomcli + ", estado=" + estado + "]";
	}
}
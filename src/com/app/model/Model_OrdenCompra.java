package com.app.model;
import java.sql.Date;
public class Model_OrdenCompra  {
	private int idOrden;
	private int idCliente;
	private Date fecha;
	private int cantidad;
	public Model_OrdenCompra() {
		super();
	}
	public Model_OrdenCompra(int idOrden, int idCliente, Date fecha, int cantidad) {
		super();
		this.idOrden = idOrden;
		this.idCliente = idCliente;
		this.fecha = fecha;
		this.cantidad = cantidad;
	}
	public int getIdOrden() {
		return idOrden;
	}
	public void setIdOrden(int idOrden) {
		this.idOrden = idOrden;
	}
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	@Override
	public String toString() {
		return "OrdenCompra [idOrden=" + idOrden + ", idCliente=" + idCliente + ", fecha=" + fecha + ", cantidad="
				+ cantidad + ", getIdCliente()=" + getIdCliente() + "]";
	}
	
	
}
package InterfazGrafica;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
public class Interfaz_ConsultasIds extends Application {
    private static final String url = "jdbc:postgresql://localhost:5432/ProyectoPAG";
    private static final String nickname = "postgres";
    private static final String contra = "1234";
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) {
        ListView<String> listView = new ListView<>();
        VBox root = new VBox(new Label("Ordenes de Artículos"), listView);
        Scene scene = new Scene(root, 400, 300);
        try {
            Connection connection = DriverManager.getConnection(url, nickname, contra);
            String query = "SELECT c.idcli, c.nomcli, a.numartic, a.nomartic " +
                           "FROM cliente c " +
                           "JOIN ordencompra o ON c.idcli = o.idcliente " +
                           "JOIN ordenarticulo oa ON o.idorden = oa.idord " +
                           "JOIN articulo a ON oa.numar = a.numartic;";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String idcli = resultSet.getString("idcli");
                String nomcli = resultSet.getString("nomcli");
                String numartic = resultSet.getString("numartic");
                String nomartic = resultSet.getString("nomartic");
                String orderInfo = "Cliente: " + idcli + " - " + nomcli + ", Artículo: " + numartic + " - " + nomartic;
                listView.getItems().add(orderInfo);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        primaryStage.setTitle("Visualizar Ordenes");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
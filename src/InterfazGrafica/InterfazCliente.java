package InterfazGrafica;
import javafx.scene.control.Alert; 
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType; 
import javafx.application.Application;
import javafx.geometry.Pos; 
import javafx.scene.Scene; 
import javafx.scene.control.*; 
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox; 
import javafx.stage.Stage; 
import java.sql.*; 
import javafx.scene.control.ButtonType;
import java.util.ArrayList; 
import java.util.List; 
import java.util.Optional; 
import com.app.model.Model_Cliente;
public class InterfazCliente extends Application {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/ProyectoPAG";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "1234";
    private TextField idTextField;
    private TextField nameTextField;
    private TextField stateTextField;
    String m1 = "  Bienvenido.";
    String m2 = " - Si desea insertar algun dato, debe llenar los campos primero y presionar el 'Insertar'";
    String m3 = " - Si desea actualizar, debe ingresar el IdCliente y los datos que se pide en los campos y presionar el boton 'Actualizar'";
    String m4 = " - Si desea eliminar solo debe llenar el campo IdCliente con el codigo existente y presionar el boton 'Eliminar'";
    String m5 = " - Si desea ver todos los datos almacenados solo presione el boton 'Mostrar Datos'";
    @Override
    public void start(Stage primaryStage) {
        Label idLabel = new Label("  IdCliente");
        Label nameLabel = new Label("  Nombre");
        Label stateLabel = new Label("  Estado");
        Label mB = new Label(m1); 
        Label mI = new Label(m2); 
        Label mA = new Label(m3); 
        Label mE = new Label(m4); 
        Label mM = new Label(m5);
        idTextField = new TextField();
        nameTextField = new TextField();
        stateTextField = new TextField();
        Button modificarButton = new Button("Insertar");
        Button actualizarButton = new Button("Actualizar");
        Button eliminarButton = new Button("Eliminar");
        Button mostrarDatosButton = new Button("Mostrar Datos");
        modificarButton.setOnAction(e -> modificarCliente());
        actualizarButton.setOnAction(e -> actualizarCliente());
        eliminarButton.setOnAction(e -> eliminarCliente());
        mostrarDatosButton.setOnAction(e -> mostrarDatosCliente());
        HBox buttonBox = new HBox(50, modificarButton, actualizarButton, eliminarButton, mostrarDatosButton);
        buttonBox.setAlignment(Pos.CENTER);
        VBox formBox = new VBox(1, mB, mI, mA, mE, mM, idLabel, idTextField, nameLabel, nameTextField, stateLabel, stateTextField);
        formBox.setAlignment(Pos.TOP_LEFT);
        Scene scene = new Scene(new VBox(formBox, buttonBox));
        primaryStage.setTitle("Registro de clientes");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    private void modificarCliente() {
        String idStr = idTextField.getText();
        String nombre = nameTextField.getText();
        String estado = stateTextField.getText();
        if (idStr.isEmpty() || nombre.isEmpty() || estado.isEmpty()) {
            mostrarAlertaDatosVacios();
            return;
        }
        int id = 0;
        try {
            id = Integer.parseInt(idStr);
        } catch (NumberFormatException e) {
            return;
        }
        try (Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String query = "INSERT INTO Cliente (idcli, nomcli, estado) VALUES (?, ?, ?)";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
            pstmt.setString(2, nombre);
            pstmt.setString(3, estado);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Éxito");
        alert.setHeaderText(null);
        alert.setContentText("El cliente fue insertado con éxito.");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            }
    }
    private void actualizarCliente() {
    	String idStr = idTextField.getText();
        String nombre = nameTextField.getText();
        String estado = stateTextField.getText();
        if (idStr.isEmpty() || nombre.isEmpty() || estado.isEmpty()) {
            mostrarAlertaDatosVacios();
            return;
        }
        int id = 0;
        try {
            id = Integer.parseInt(idStr);
        } catch (NumberFormatException e) {
            return;
        }
        try (Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String query = "UPDATE cliente SET nomcli = ?, estado = ? WHERE idcli = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, nombre);
            pstmt.setString(2, estado);
            pstmt.setInt(3, id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Éxito");
        alert.setHeaderText(null);
        alert.setContentText("El cliente fue actualizado con éxito.");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            }
    }
    private void eliminarCliente() {
        String idStr = idTextField.getText();
        if (idStr.isEmpty()) {
            mostrarAlertaDatosVacios();
            return;
        }
        int id = 0;
        try {
            id = Integer.parseInt(idStr);
        } catch (NumberFormatException e) {
            return;
        }
        try (Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String query = "DELETE FROM Cliente WHERE idcli = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id); 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Éxito");
        alert.setHeaderText(null);
        alert.setContentText("El cliente fue eliminado con éxito.");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            }
    }
    private List<Model_Cliente> obtenerDatosDeLaBaseDeDatos() {
        List<Model_Cliente> datosClientes = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String query = "SELECT idcli, nomcli, estado FROM Cliente";
            PreparedStatement pstmt = conn.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("idcli");
                String nombre = rs.getString("nomcli");
                String estado = rs.getString("estado");
                Model_Cliente cliente = new Model_Cliente(id, nombre, estado);
                datosClientes.add(cliente);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return datosClientes;
    }
    private void mostrarDatosCliente() {
        List<Model_Cliente> clientes = obtenerDatosDeLaBaseDeDatos();
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Datos de Clientes");
        alert.setHeaderText(null);
        StringBuilder content = new StringBuilder();
        content.append("Codigo         Nombre               Estado\n");
        for (Model_Cliente cliente : clientes) {
            content.append(" ").append(cliente.getIdcli());
            content.append("                ").append(cliente.getNomcli());
            content.append("                ").append(cliente.getEstado());
            content.append("\n");
        }
        alert.setContentText(content.toString());
        alert.showAndWait();
    }
    private void mostrarAlertaDatosVacios() {
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Advertencia");
        alert.setHeaderText(null);
        alert.setContentText("Por favor, complete los campos antes de continuar.");
        alert.showAndWait();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
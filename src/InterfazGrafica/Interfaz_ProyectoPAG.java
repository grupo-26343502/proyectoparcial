package InterfazGrafica;
import javafx.application.Application;
import javafx.scene.control.Label;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.geometry.Insets;
public class Interfaz_ProyectoPAG extends Application {
    public static void main(String[] args) {
        launch(args);
    }
    /* Integrantes:
    Tirado Mendoza Kelvin Aarón
    Abarca Velazco Bryan Andre
    Macias Carranza Krystel Shanely
    Suárez Barco José Enrique
    Zambrano Mendoza Jeremy Dario
    */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Opciones de registro");
        Label mensajeB = new Label("Bienvenido.\nEscoja la accion que desea realizar:");
        Button customerButton = new Button("Menú Clientes");
        Button inventarioButton = new Button("Menú Articulos");
        Button ordenesButton = new Button("Menú de Ordenes de compra");
        Button salirButton = new Button("Cerrar sesion");
        customerButton.setOnAction(e -> openCustomerForm());
        inventarioButton.setOnAction(e -> openInventarioApp());
        ordenesButton.setOnAction(e -> openOrdenesApp());
        salirButton.setOnAction(e -> primaryStage.close()); 
        VBox vbox = new VBox(10, mensajeB,customerButton, inventarioButton, ordenesButton, salirButton);
        vbox.setPadding(new Insets(10));
        Scene scene = new Scene(vbox, 300, 200);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    private void openCustomerForm() {
        InterfazCliente customerForm = new InterfazCliente();
        Stage stage = new Stage();
        customerForm.start(stage);
    }
    private void openInventarioApp() {
        InterfazArticulo inventarioApp = new InterfazArticulo();
        Stage stage = new Stage();
        inventarioApp.start(stage);
    }
    private void openOrdenesApp() {
        InterfazOrdenCompra ordenesApp = new InterfazOrdenCompra();
        Stage stage = new Stage();
        ordenesApp.start(stage);
    }
}

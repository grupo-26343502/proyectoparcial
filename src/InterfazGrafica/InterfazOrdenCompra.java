package InterfazGrafica;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import java.sql.*;
import java.time.LocalDate;
public class InterfazOrdenCompra extends Application {
	private TextField idOrdenField;
    private TextField idClienteField; 
    private DatePicker fechaPicker;
    private TextField cantidadField;
    private TextArea datosTextArea;
    String m1 = "  Bienvenido.";
    String m2 = " - Si desea insertar algun dato, debe llenar los campos primero y presionar el 'Insertar'";
    String m3 = " - Si desea actualizar, debe ingresar el IdOrden existente y los datos que se pide en los campos y presionar el boton 'Actualizar'";
    String m4 = " - Si desea eliminar solo debe llenar el campo IdOrden con el codigo existente y presionar el boton 'Eliminar'";
    String m5 = " - Si desea ver todos los datos almacenados solo presione el boton 'Mostrar'";
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Ordenes de Compra");
        Label mB = new Label(m1); 
        Label mI = new Label(m2); 
        Label mA = new Label(m3); 
        Label mE = new Label(m4); 
        Label mM = new Label(m5);
        idOrdenField = new TextField();
        idClienteField = new TextField();
        fechaPicker = new DatePicker();
        cantidadField = new TextField();
        datosTextArea = new TextArea();
        datosTextArea.setEditable(false);
        Button insertarButton = new Button("Insertar");
        Button actualizarButton = new Button("Actualizar");
        Button eliminarButton = new Button("Eliminar");
        Button mostrarButton = new Button("Mostrar");
        Button limpiarButton = new Button("Limpiar");
        limpiarButton.setOnAction(e -> limpiarCampos());
        insertarButton.setOnAction(e -> insertarDatos());
        actualizarButton.setOnAction(e -> actualizarDatos());
        eliminarButton.setOnAction(e -> eliminarDatos());
        mostrarButton.setOnAction(e -> mostrarDatos());
        HBox hbox = new HBox(5,
                new Label(" IdOrden:"), idOrdenField,
                new Label(" IdCliente:"), idClienteField,
                new Label(" Fecha:"), fechaPicker,
                new Label(" Cantidad:"), cantidadField,
                insertarButton, mostrarButton, actualizarButton, eliminarButton, limpiarButton);
        VBox vbox = new VBox(5, mB,mI,mA,mE,mM,
        		hbox,
                new Label("Datos almacenados:"), datosTextArea);
        vbox.setPadding(new Insets(10));
        vbox.setAlignment(Pos.CENTER_LEFT);
        Scene scene = new Scene(vbox, 1200, 350);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    private Connection getConnection() throws SQLException {
        String dbUrl = "jdbc:postgresql://localhost:5432/ProyectoPAG";
        String dbUser = "postgres";
        String dbPassword = "1234";
        return DriverManager.getConnection(dbUrl, dbUser, dbPassword);
    }
    private void insertarDatos() {
        String idOrdenStr = idOrdenField.getText();
        String idClienteStr = idClienteField.getText();
        LocalDate fecha = fechaPicker.getValue();
        String cantidadStr = cantidadField.getText();
        if (idOrdenStr.isEmpty() || idClienteStr.isEmpty() || fecha == null || cantidadStr.isEmpty()) {
            mostrarAlertaDatosVacios();
            return;
        }
        int idOrden = Integer.parseInt(idOrdenStr);
        int idCliente = Integer.parseInt(idClienteStr);
        int cantidad = Integer.parseInt(cantidadStr);
        try (Connection conn = getConnection()) {
            String query = "INSERT INTO ordencompra (idorden, idcliente, fecha, cantidad) VALUES (?, ?, ?, ?)";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, idOrden);
            pstmt.setInt(2, idCliente);
            pstmt.setDate(3, Date.valueOf(fecha));
            pstmt.setInt(4, cantidad);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String datosIngresados = "IdOrden: " + idOrden +
                ", IdCliente: " + idCliente + 
                ", Fecha: " + fecha +
                ", Cantidad: " + cantidad + "\n";
        datosTextArea.appendText(datosIngresados);
    }
    private void actualizarDatos() {
    	String idOrdenStr = idOrdenField.getText();
        String idClienteStr = idClienteField.getText();
        LocalDate fecha = fechaPicker.getValue();
        String cantidadStr = cantidadField.getText();
        if (idOrdenStr.isEmpty() || idClienteStr.isEmpty() || fecha == null || cantidadStr.isEmpty()) {
            mostrarAlertaDatosVacios();
            return;
        }
        int idOrden = Integer.parseInt(idOrdenStr);
        int idCliente = Integer.parseInt(idClienteStr);
        int cantidad = Integer.parseInt(cantidadStr);
        try (Connection conn = getConnection()) {
            String query = "UPDATE ordencompra SET idcliente = ?, fecha = ?, cantidad = ? WHERE idorden = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, idCliente); 
            pstmt.setDate(2, Date.valueOf(fecha));
            pstmt.setInt(3, cantidad);
            pstmt.setInt(4, idOrden);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String datosActualizados = "IdOrden: " + idOrden +
                ", IdCliente: " + idCliente + 
                ", Fecha: " + fecha +
                ", Cantidad: " + cantidad + "\n";
        datosTextArea.appendText(datosActualizados);
    }
    private void eliminarDatos() {
        String idOrdenStr = idOrdenField.getText();
        if (idOrdenStr.isEmpty()) {
            mostrarAlertaDatosVacios();
            return;
        }
        int idOrden = Integer.parseInt(idOrdenStr);
        try (Connection conn = getConnection()) {
            String query = "DELETE FROM ordencompra WHERE idorden = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, idOrden);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private void mostrarDatos() {
        try (Connection conn = getConnection()) {
            String query = "SELECT o.idorden, o.fecha, o.cantidad, c.idcli " +
                           "FROM ordencompra o " +
                           "JOIN cliente c ON o.idcliente = c.idcli";
            PreparedStatement pstmt = conn.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();
            StringBuilder content = new StringBuilder();
            content.append("Codigo Orden         Codigo Cliente               Fecha             Cantidad\n");
            while (rs.next()) {
                int idOrden = rs.getInt("idorden");
                LocalDate fecha = rs.getDate("fecha").toLocalDate();
                int cantidad = rs.getInt("cantidad");
                int idCliente = rs.getInt("idcli");
                content.append("  ").append(idOrden);
                content.append("                                  ").append(idCliente); 
                content.append("                          ").append(fecha);
                content.append("              ").append(cantidad);
                content.append("\n");
            }
            datosTextArea.setText(content.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private void limpiarCampos() {
        idOrdenField.clear();
        idClienteField.clear(); 
        fechaPicker.setValue(null);
        cantidadField.clear();
    }
    private void mostrarAlertaDatosVacios() {
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Advertencia");
        alert.setHeaderText(null);
        alert.setContentText("Por favor, complete todos los campos antes de continuar.");
        alert.showAndWait();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
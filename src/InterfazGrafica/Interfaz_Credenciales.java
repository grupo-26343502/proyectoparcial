package InterfazGrafica;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Interfaz_Credenciales extends Application {
    private TextField usuario;
    private PasswordField contrasena;
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Inicie sesion");
        usuario = new TextField();
        contrasena = new PasswordField();
        Button loginButton = new Button("Ingresar");
        loginButton.setOnAction(e -> {
            if (Usu_ContraCorrecta(usuario.getText(), contrasena.getText())) {
                openInterfaz_ProyectoPAG();
            } else {
                showErrorMessage("Usuario y/o contraseña incorrecta");
            }
        });
        Button salirButton = new Button("Salir");
        salirButton.setOnAction(e -> {
            Platform.exit();
        });
        HBox buttonsBox = new HBox(10, loginButton, salirButton);
        buttonsBox.setAlignment(Pos.CENTER);
        VBox vbox = new VBox(10, new Label("Usuario:"), usuario, new Label("Contraseña:"), contrasena, buttonsBox);
        vbox.setPadding(new Insets(10));
        Pane root = new Pane();
        root.getChildren().add(vbox);
        Scene scene = new Scene(root, 190, 200);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    private boolean Usu_ContraCorrecta(String username, String password) {
        try (Connection conn = getConnection()) {
            String query = "SELECT * FROM credenciales WHERE usuario = ? AND contrasena = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            ResultSet rs = pstmt.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    private Connection getConnection() throws SQLException {
        String dbUrl = "jdbc:postgresql://localhost:5432/ProyectoPAG";
        String dbUser = "postgres";
        String dbPassword = "1234";
        return DriverManager.getConnection(dbUrl, dbUser, dbPassword);
    }
    private void openInterfaz_ProyectoPAG() {
        Interfaz_ProyectoPAG mainApp = new Interfaz_ProyectoPAG();
        Stage stage = new Stage();
        mainApp.start(stage);
    }
    private void showErrorMessage(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
}
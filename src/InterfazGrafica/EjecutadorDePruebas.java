package InterfazGrafica;

import com.app.dao.CredDaoImp;
import com.app.dao.CredenciaDaoLog;
import com.app.model.Model_CredencialesLogin;

public class EjecutadorDePruebas {
    public static void main(String[] args) {
        CredDaoImp usuarioDAO = new CredenciaDaoLog();
        Model_CredencialesLogin credencial = usuarioDAO.login("Katm24", "Mend24");

        if (credencial != null) {
            System.out.println("Bienvenido al sistema");
        } else {
            System.out.println("Usuario o contraseña incorrecta");
        }
    }
}

package InterfazGrafica;	
import javafx.application.Application;
import javafx.scene.control.Label;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.geometry.Pos;
public class InterfazArticulo extends Application {
    private TextField numeroArticuloField;
    private TextField nombreArticuloField;
    private TextField precioField;
    private TextArea datosTextArea;
    String m1 = "Bienvenido.\nEscoja la accion que desea realizar:";
    String m2 = "- Si desea insertar algun dato, debe llenar los campos primero y presionar el 'Insertar'";
    String m3 = "- Si desea actualizar, debe ingresar el codigo y los datos que se pide en los campos y presionar el boton 'Actualizar'";
    String m4 = "- Si desea eliminar solo debe llenar el campo No Articulo con el codigo existente y presionar el boton 'Eliminar'";
    String m5 = "- Si desea ver todos los datos almacenados solo presione el boton 'Mostrar Datos'\n ";
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Registro de Articulos");
        Label mB = new Label(m1);
        Label mI = new Label(m2);
        Label mA = new Label(m3);
        Label mE = new Label(m4);
        Label mM = new Label(m5);
        numeroArticuloField = new TextField();
        nombreArticuloField = new TextField();
        precioField = new TextField();
        datosTextArea = new TextArea();
        datosTextArea.setEditable(false);
        Button insertarButton = new Button("Insertar");
        Button actualizarButton = new Button("Actualizar");
        Button eliminarButton = new Button("Eliminar");
        Button mostrarDatosButton = new Button("Mostrar Datos");
        Button limpiarButton = new Button("Limpiar");
        limpiarButton.setOnAction(e -> limpiarCampos());
        insertarButton.setOnAction(e -> insertarDatos());
        actualizarButton.setOnAction(e -> actualizarDatos());
        eliminarButton.setOnAction(e -> eliminarDatos());
        mostrarDatosButton.setOnAction(e -> mostrarDatosArticulos());
        HBox hbox = new HBox(10,
                new Label("N° Articulo:"), numeroArticuloField,
                new Label("Nombre Articulo:"), nombreArticuloField,
                new Label("Precio:"), precioField,
                insertarButton, actualizarButton, eliminarButton, mostrarDatosButton,limpiarButton);

        VBox vbox = new VBox(10,
        		mB,mI,mA,mE,mM,
        		hbox,
                new Label("Datos almacenados:"), datosTextArea);
        vbox.setPadding(new Insets(10));
        vbox.setAlignment(Pos.CENTER_LEFT);
        Scene scene = new Scene(vbox, 1100, 500);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    private Connection getConnection() throws SQLException {
        String dbUrl = "jdbc:postgresql://localhost:5432/ProyectoPAG";
        String dbUser = "postgres";
        String dbPassword = "1234";
        return DriverManager.getConnection(dbUrl, dbUser, dbPassword);
    }
    private void insertarDatos() {
        String numeroArticuloStr = numeroArticuloField.getText();
        String nombreArticulo = nombreArticuloField.getText();
        String precioStr = precioField.getText();
        if (numeroArticuloStr.isEmpty() || nombreArticulo.isEmpty() || precioStr.isEmpty()) {
            mostrarAlertaDatosVacios();
            return;
        }
        int numeroArticulo = Integer.parseInt(numeroArticuloStr);
        double precio = Double.parseDouble(precioStr);
        try (Connection conn = getConnection()) {
            String query = "INSERT INTO articulo (numartic, nomartic, precio) VALUES (?, ?, ?)";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, numeroArticulo);
            pstmt.setString(2, nombreArticulo);
            pstmt.setDouble(3, precio);
            pstmt.executeUpdate();
            datosTextArea.appendText("Datos insertados con éxito.\n");
        } catch (SQLException e) {
            e.printStackTrace();
            datosTextArea.appendText("Error al insertar los datos.\n");
        }
    }
    private void actualizarDatos() {
    	String numeroArticuloStr = numeroArticuloField.getText();
        String nombreArticulo = nombreArticuloField.getText();
        String precioStr = precioField.getText();
        if (numeroArticuloStr.isEmpty() || nombreArticulo.isEmpty() || precioStr.isEmpty()) {
            mostrarAlertaDatosVacios();
            return;
        }
        int numeroArticulo = Integer.parseInt(numeroArticuloStr);
        double precio = Double.parseDouble(precioStr);
        try (Connection conn = getConnection()) {
            String query = "UPDATE articulo SET nomartic = ?, precio = ? WHERE numartic = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, nombreArticulo);
            pstmt.setDouble(2, precio);
            pstmt.setInt(3, numeroArticulo);
            int rowsUpdated = pstmt.executeUpdate();
            if (rowsUpdated > 0) {
                datosTextArea.appendText("Articulo actualizado con éxito.\n");
            } else {
                datosTextArea.appendText("No se encontró el articulo con el número especificado.\n");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            datosTextArea.appendText("Error al actualizar el articulo.\n");
        }
    }
    private void eliminarDatos() {
        String numeroArticuloStr = numeroArticuloField.getText();
        if (numeroArticuloStr.isEmpty()) {
            mostrarAlertaDatosVacios();
            return;
        }
        int numeroArticulo = Integer.parseInt(numeroArticuloStr);
        try (Connection conn = getConnection()) {
            String query = "DELETE FROM articulo WHERE numartic = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, numeroArticulo);
            int rowsDeleted = pstmt.executeUpdate();
            if (rowsDeleted > 0) {
                datosTextArea.appendText("Articulo eliminado con éxito.\n");
            } else {
                datosTextArea.appendText("No se encontró el articulo con el número especificado.\n");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            datosTextArea.appendText("Error al eliminar el articulo.\n");
        }
    }
    private void mostrarDatosArticulos() {
        try (Connection conn = getConnection()) {
            String query = "SELECT numartic, nomartic, precio FROM Articulo";
            PreparedStatement pstmt = conn.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();
            StringBuilder content = new StringBuilder();
            content.append("   Codigo                            Nombre                   Precio\n");
            while (rs.next()) {
                int numeroArticulo = rs.getInt("numartic");
                String nombreArticulo = rs.getString("nomartic");
                double precio = rs.getDouble("precio");
                content.append("N° Articulo: ").append(numeroArticulo);
                content.append("_____________");
                content.append("").append(nombreArticulo);
                content.append("_____________");
                content.append("").append(precio);
                content.append("\n");
            }
            datosTextArea.setText(content.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            datosTextArea.setText("Error al obtener los datos.\n");
        }
    }
    private void limpiarCampos() {
        numeroArticuloField.clear();
        nombreArticuloField.clear();
        precioField.clear();
    }
    private void mostrarAlertaDatosVacios() {
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Advertencia");
        alert.setHeaderText(null);
        alert.setContentText("Por favor, complete todos los campos antes de continuar.");
        alert.showAndWait();
    }
    public static void main(String[] args) {
        launch(args);
    }
}

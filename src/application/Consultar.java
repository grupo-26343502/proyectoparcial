package application;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Consultar extends Application {
    private Connection conn;
    private TextField jTextConsultar;
    private TextField jTextID;
    private TextField jTextNombre;
    private TextField jTextEstado;
    @Override
    public void start(Stage primaryStage) {
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        Label labelConsultar = new Label("Consultar con IdCliente:");
        jTextConsultar = new TextField();
        Button buttonAceptar = new Button("Aceptar");
        Button buttonCancelar = new Button("Cancelar");
        Label labelID = new Label("ID:");
        jTextID = new TextField();
        jTextID.setEditable(false);
        Label labelNombre = new Label("Nombre:");
        jTextNombre = new TextField();
        jTextNombre.setEditable(false);
        Label labelEstado = new Label("Estado:");
        jTextEstado = new TextField();
        jTextEstado.setEditable(false);
        buttonAceptar.setOnAction(e -> consultarCliente());
        buttonCancelar.setOnAction(e -> primaryStage.close());
        gridPane.add(labelConsultar, 0, 0);
        gridPane.add(jTextConsultar, 1, 0);
        gridPane.add(buttonAceptar, 2, 0);
        gridPane.add(buttonCancelar, 3, 0);
        gridPane.add(labelID, 0, 1);
        gridPane.add(jTextID, 1, 1);
        gridPane.add(labelNombre, 0, 2);
        gridPane.add(jTextNombre, 1, 2);
        gridPane.add(labelEstado, 0, 3);
        gridPane.add(jTextEstado, 1, 3);
        Scene scene = new Scene(gridPane, 500, 200);
        primaryStage.setTitle("Consulta de Cliente");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    private void consultarCliente() {
        int id = 0;
        String nombre = "", estado = "";

        String ced = jTextConsultar.getText();
        try {
            conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ProyectoPAG", "postgres", "1234");
            System.out.println("Conexión exitosa a la base de datos");

            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM public.cliente WHERE idcli = ?");
            stmt.setString(1, ced);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                id = rs.getInt("idcliente");
                nombre = rs.getString("nombre");
                estado = rs.getString("estado");
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (id != 0) {
            String ide = Integer.toString(id);
            jTextID.setText(ide);
            jTextNombre.setText(nombre);
            jTextEstado.setText(estado);
        } else {
            showMessageDialog(null, "No se encuentra en la base de datos");
            vaciar();
        }
    }
    private void showMessageDialog(Object object, String string) {
        // TODO Auto-generated method stub
    }
    public void vaciar() {
        jTextID.setText("");
        jTextNombre.setText("");
        jTextEstado.setText("");
    }
    public static void main(String[] args) {
        launch(args);
    }
}
module ProyectoPAG {
	requires javafx.controls;
	requires java.sql;
	opens com.app.model;
	exports InterfazGrafica;
	opens application to javafx.base,fx.graphics, javafx.fxml;
}